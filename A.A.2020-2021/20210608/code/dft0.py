import numpy as np
import matplotlib.pyplot as plt

fc = 1000.0
sinc = 1/fc
fcnyquist = fc/2.0
dur = 30
f0 = 30.53

x = np.linspace(-dur/2.0,(dur/2.0)-sinc,int(fc*dur))
f = np.linspace(-fcnyquist, fcnyquist-sinc, int(fc))

y = np.exp(1j*2*np.pi*f0*x)

ffty = []

for k in f:
    analfun = np.exp(-1j*2*np.pi*k*x)
    analmul = analfun * y
    ffty.append(np.sum(analmul))

ffty = np.array(ffty)
fftymag = np.abs(ffty)/x.size

plt.figure(figsize=(14,8))
plt.subplot(121)
plt.plot(x, y.real, x, y.imag)
plt.axis([-0.05, 0.05, -1.1, 1.1])
plt.subplot(122)
plt.stem(f, fftymag)
plt.axis([-50.0, 50.0, -0.1, 1.1])
plt.savefig("./images/dft0.png", format="png")
plt.show()
