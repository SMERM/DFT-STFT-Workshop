import numpy as np
import matplotlib.pyplot as plt

fc = 1000.0
sinc = 1/fc
fcnyquist = fc/2.0
dur = 30
f0 = 148
dftsize=255
dftbinsize = fc/dftsize

x = np.linspace(-dur/2.0,(dur/2.0)-sinc, int(fc*dur))
f = np.linspace(-fcnyquist, fcnyquist-dftbinsize, int(dftsize))

y = np.cos(2*np.pi*f0*x)

ffty = []

for k in f:
    analfun = np.exp(-1j*2*np.pi*k*x)
    analmul = analfun * y
    ffty.append((np.sum(analmul))/x.size)

ffty = np.array(ffty)
fftymag = np.abs(ffty)

plt.figure(figsize=(14,8))
plt.stem(f, fftymag)
plt.savefig("./images/dft3.png", format="png")
plt.show()
