import numpy as np
import matplotlib.pyplot as plt

fc = 1000.0
sinc = 1/fc
dur = 30.0
f0 = 1.5
f1 = 0.12

x = np.linspace(-dur/2.0,(dur/2.0)-sinc,int(fc*dur))
y0 = np.cos(np.pi*2*f0*x)
y1 = np.cos(np.pi*2*f1*x)

yresult0 = y0 * y1

ysum0 = (np.sum(yresult0))/x.size

yresult1 = y1 * y1
ysum1 = (np.sum(yresult1))/x.size

plt.figure(figsize=(14,8))
plt.subplot(121)
plt.plot(x, yresult0)
plt.ylabel("%.4f" % (ysum0))
plt.subplot(122)
plt.plot(x, yresult1)
plt.ylabel("%.4f" % (ysum1))
plt.axis([-dur/2.0, dur/2.0, -1, 1])
plt.savefig("./images/period_multip.png", format="png")
plt.show()
