import numpy as np
import matplotlib.pyplot as plt


x = np.linspace(-2*np.pi, 2*np.pi, 500)
y = np.exp(1j*x)

plt.plot(x, y.real, x, y.imag)
plt.savefig("./images/eulero.png", format="png")
plt.show()
