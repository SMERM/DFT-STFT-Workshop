# Bernardini - Seminario su DFT e STFT, Ep.1 20210608

[YouTube Video](https://youtu.be/hiRbBtOtkNg)

![Jean-Baptiste Joseph Fourier](./images/Fourier2.jpg) Un bel tomo.

## Argomenti

### Breve ripasso della funzionalità del dominio dei numeri complessi

![numeri complessi](./images/complex.png)

[Illustrazione della formula di Eulero](./code/eulero.py):

```python
import numpy as np
import matplotlib.pyplot as plt


x = np.linspace(-2*np.pi, 2*np.pi, 500)
y = np.exp(1j*x)

plt.plot(x, y.real, x, y.imag)
plt.savefig("./images/eulero.png", format="png")
plt.show()
```

Il plot illustra la parte reale e la parte immaginaria separatamente:

![Formula di Eulero](./images/eulero.png)

### Introduzione e funzionalita della scomposizione in serie di Fourier

* Similitudini tra le funzioni dell'algebra lineare e quelle della scomposizione in serie di Fourier

![Scomposizione in serie di Fourier](./images/fourier_decomposition.png)

#### L'idea di Fourier:

* calcolando l'area di due funzioni periodiche semplici tra meno infinito e
  più infinito, questa risulterà zero per qualsiasi frequenza **salvo**
  quando le due frequenze sono uguali
```python
import numpy as np
import matplotlib.pyplot as plt

fc = 1000.0
sinc = 1/fc
dur = 30.0
f0 = 1.5
f1 = 0.12

x = np.linspace(-dur/2.0,(dur/2.0)-sinc,int(fc*dur))
y0 = np.cos(np.pi*2*f0*x)
y1 = np.cos(np.pi*2*f1*x)

yresult0 = y0 * y1

ysum0 = (np.sum(yresult0))/x.size

yresult1 = y1 * y1
ysum1 = (np.sum(yresult1))/x.size

plt.figure(figsize=(14,8))
plt.subplot(121)
plt.plot(x, yresult0)
plt.ylabel("%.4f" % (ysum0))
plt.subplot(122)
plt.plot(x, yresult1)
plt.ylabel("%.4f" % (ysum1))
plt.axis([-dur/2.0, dur/2.0, -1, 1])
plt.show()
```

![moltiplicazione di funzioni complesse](./images/period_multip.png)


* quindi: facendo la moltiplicazione e la sommatoria di *tutte* le possibili frequenze 
  si metteranno in luce quelle presenti nella funzione analizzata

#### [DFT complessa](./code/dft0.py)

```python
import numpy as np
import matplotlib.pyplot as plt

fc = 1000.0
sinc = 1/fc
fcnyquist = fc/2.0
dur = 30
f0 = 30.53

x = np.linspace(-dur/2.0,(dur/2.0)-sinc,int(fc*dur))
f = np.linspace(-fcnyquist, fcnyquist-sinc, int(fc))

y = np.exp(1j*2*np.pi*f0*x)

ffty = []

for k in f:
    analfun = np.exp(-1j*2*np.pi*k*x)
    analmul = analfun * y
    ffty.append(np.sum(analmul))

ffty = np.array(ffty)
fftymag = np.abs(ffty)/x.size

plt.figure(figsize=(14,8))
plt.subplot(121)
plt.plot(x, y.real, x, y.imag)
plt.axis([-0.05, 0.05, -1.1, 1.1])
plt.subplot(122)
plt.stem(f, fftymag)
plt.axis([-50.0, 50.0, -0.1, 1.1])
plt.show()
```

```sh
$ python3 code/dft0.py
```

![dft 0](./images/dft0.png)

#### [DFT complessa con più componenti riscalate](./code/dft1.py)

```python
import numpy as np
import matplotlib.pyplot as plt

fc = 1000.0
sinc = 1/fc
fcnyquist = fc/2.0
dur = 30
f0 =  9.509
f1 = 30.53
amp0 = 0.8
amp1 = 0.25

x = np.linspace(-dur/2.0,(dur/2.0)-sinc, int(fc*dur))
f = np.linspace(-fcnyquist, fcnyquist-sinc, int(fc))

y = amp0*np.exp(1j*2*np.pi*f0*x) + amp1*np.exp(1j*2*np.pi*f1*x)

ffty = []

for k in f:
    analfun = np.exp(-1j*2*np.pi*k*x)
    analmul = analfun * y
    ffty.append(np.sum(analmul))

ffty = np.array(ffty)
fftymag = np.abs(ffty)/x.size

plt.figure(figsize=(14,8))
plt.subplot(121)
plt.plot(x, y.real, x, y.imag)
plt.axis([-0.05, 0.05, -1.1, 1.1])
plt.subplot(122)
plt.stem(f, fftymag)
plt.axis([-50.0, 50.0, -0.1, 1.1])
plt.savefig("./images/dft1.png", format="png")
plt.show()
```

```sh
$ python3 code/dft1.py
```

![DFT 1](./images/dft1.png)

#### [DFT reale](./code/dft2.py)

```python
import numpy as np
import matplotlib.pyplot as plt

fc = 1000.0
sinc = 1/fc
fcnyquist = fc/2.0
dur = 30
f0 = 30.53

x = np.linspace(-dur/2.0,(dur/2.0)-sinc, int(fc*dur))
f = np.linspace(-fcnyquist, fcnyquist-sinc,  int(fc))

y = np.cos(2*np.pi*f0*x)

ffty = []

for k in f:
    analfun = np.exp(-1j*2*np.pi*k*x)
    analmul = analfun * y
    ffty.append(np.sum(analmul))

ffty = np.array(ffty)
fftymag = np.abs(ffty)/x.size

plt.figure(figsize=(14,8))
plt.subplot(121)
plt.plot(x, y)
plt.axis([-0.05, 0.05, -1.1, 1.1])
plt.subplot(122)
plt.stem(f, fftymag)
plt.axis([-50.0, 50.0, -0.1, 1.1])
plt.savefig("./images/dft2.png", format="png")
plt.show()
```

```sh
$ python3 code/dft2.py
```

![DFT 2](./images/dft2.png)

Il risultato è conforme alla formula di Eulero:

![Formula di Eulero](./images/eulero.png)

considerando che:

![Reale vs Complesso](./images/real_vs_complex.png)

#### [DFT reale su frequenze non allineate con i bins](./code/dft3.png)

```python
import numpy as np
import matplotlib.pyplot as plt

fc = 1000.0
sinc = 1/fc
fcnyquist = fc/2.0
dur = 30
f0 = 148
dftsize=255
dftbinsize = fc/dftsize

x = np.linspace(-dur/2.0,(dur/2.0)-sinc, int(fc*dur))
f = np.linspace(-fcnyquist, fcnyquist-dftbinsize, int(dftsize))

y = np.cos(2*np.pi*f0*x)

ffty = []

for k in f:
    analfun = np.exp(-1j*2*np.pi*k*x)
    analmul = analfun * y
    ffty.append((np.sum(analmul))/x.size)

ffty = np.array(ffty)
fftymag = np.abs(ffty)

plt.figure(figsize=(14,8))
plt.stem(f, fftymag)
plt.savefig("./images/dft3.png", format="png")
plt.show()
```

```sh
$ python3 code/dft2.py
```

![DFT 3](./images/dft3.png)

poiché

![problemi con il DFT reale](./images/real_world_dft.png)
