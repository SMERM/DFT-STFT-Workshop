# DFT STFT Workshop (in italian)

Un laboratorio dedicato alla trasformata  discreta  di  Fourier  e  alla
trasformata di Fourier sul tempo breve, dedicato con amore a  tutti  gli
studenti della Scuola di Musica Elettronica di  Santa  Cecilia  e oltre.

## Strumenti utilizzati

* `python3`
* `numpy`
* `scipy`
* `matplotlib`
